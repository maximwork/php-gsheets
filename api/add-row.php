<?
header("Access-Control-Allow-Origin: *");

//$allowed_domains = [
//    'https://q-platform.ru',
//    'https://test.q-platform.ru',
//    'https://demo.q-platform.ru',
//];
//
//if (in_array($_SERVER['HTTP_ORIGIN'], $allowed_domains))
//    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);


use Service\Spreadsheets\Spreadsheets;

require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $requestData = json_decode(file_get_contents("php://input"), true);
    $requestData = $requestData ? $requestData : $_POST;

    $spreadsheets = new Spreadsheets();

    $data = $spreadsheets->mapRequestData($requestData);
    $spreadsheets->add($data);

    die(json_encode($requestData));
}