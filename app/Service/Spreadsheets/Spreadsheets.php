<?php
namespace Service\Spreadsheets;

use Google_Client;
use Google_Service_Sheets;
use Google_Service_Sheets_ValueRange;

require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

class Spreadsheets
{
    private const SPREADSHEET_ID = '1MeSR2KSAASvnoe091ZCd1kdmcxypNMsDI-4Fp42VfU0'; // id from sheet url
    private const ACCOUNT_KEY_FILE_PATH = '/assets/qplatform-301623-2b9f66ccd634.json'; // service account credentials generated from console.cloud.google.com
    private const SHEET_LIST_NAME = 'Заявки';
    private const SHEET_LIST_DIMENSIONS = 'A:AG';
    private const PRODUCT_LAT_TO_CYR = [
        'product:KK' => 'Кредитный конвейер',
        'product:OCR' => 'Распознавание документов (OCR)',
        'product:ScoringModels' => 'Создание скоринговых моделей',
        'product:SalaryProject' => 'Зарплатный проект',
        'product:BrokerageSolutions' => 'Биржевые продукты через API',
    ];
    private const COL_ORDER = [
        'gtm_form_send_contact_id', // A - id заявки
        'gtm_form_send_site', // B - Сайт
        'gtm_form_send_product_or_service', // C - Продукт/сервис
        'gtm_form_send_page', // D - Страница контакта
        'gtm_form_send_form_type', // E - Тип формы
        'gtm_form_send_calc_source', // F - Источник
        'gtm_form_send_calc_medium', // G - Канал
        'gtm_form_send_utm_source', // H - Source
        'gtm_form_send_utm_medium', // I - Medium
        'gtm_form_send_utm_campaign', // J - Campaign
        'gtm_form_send_utm_content', // K - Content
        'gtm_form_send_utm_term', // L - Term
        'gtm_form_send_date', // M - Дата
        'firstName', // N - Имя
        'lastName', // O - Фамилия
        'email', // P - Email
        'phone', // Q - Телефон
        'companyName', // R - Компания
        'position', // S - Должность
        'comment', // T - Комментарий
        'gtm_form_send_client_id', // U - Client id
    ];
    private $googleAccountKeyFilePath;
    private $spreadsheetId;
    private $client;
    private $service;
    private $listName;
    private $listDimensions;
    private $listRange;
    public function __construct()
    {
        $this->spreadsheetId = self::SPREADSHEET_ID;
        $this->googleAccountKeyFilePath = $_SERVER['DOCUMENT_ROOT'] . self::ACCOUNT_KEY_FILE_PATH;
        putenv( 'GOOGLE_APPLICATION_CREDENTIALS=' . $this->googleAccountKeyFilePath );
        $this->listName = self::SHEET_LIST_NAME;
        $this->listDimensions = self::SHEET_LIST_DIMENSIONS;
        $this->listRange = $this->listName .'!'. $this->listDimensions;
        $this->initClient();
        $this->initService();
    }
    public function mapRequestData($requestData)
    {
        $data = [];

        foreach (self::COL_ORDER as $key) {

            if ($key === 'gtm_form_send_product_or_service') {

                if (isset($requestData['serviceName']) && $requestData['serviceName']) {

                    $data[] = $requestData['serviceName'];

                } else if ($requestData['gtm_form_send_form_type'] &&
                    strpos($requestData['gtm_form_send_form_type'], 'product') !== false) {

                    $data[] = self::PRODUCT_LAT_TO_CYR[$requestData['gtm_form_send_form_type']];

                    $requestData['gtm_form_send_form_type'] = 'product';

                } else {

                    $data[] = isset($requestData[$key]) ? htmlspecialchars($requestData[$key]) : '';
                }
            } else {

                $data[] = isset($requestData[$key]) ? htmlspecialchars($requestData[$key]) : '';
            }
        }
        return [$data];
    }

    public function add($data)
    {
        $body = new Google_Service_Sheets_ValueRange(['values' => $data]);
        $options = array( 'valueInputOption' => 'RAW' );
        $nextRow = $this->getGetNextRowNum();
        $this->service->spreadsheets_values->update(
            $this->spreadsheetId,
            $this->listName .'!A'. $nextRow,
            $body,
            $options
        );
    }
    public function getGetNextRowNum()
    {
        return count($this->getValues()['values']) + 1;
    }
    public function getValues()
    {
        return $this->service->spreadsheets_values->get($this->spreadsheetId, $this->listRange);
    }
    private function initService()
    {
        $this->service = new Google_Service_Sheets( $this->client );
    }
    private function initClient()
    {
        $this->client = new Google_Client();
        $this->client->useApplicationDefaultCredentials();
        $this->client->addScope( 'https://www.googleapis.com/auth/spreadsheets' );
    }
}